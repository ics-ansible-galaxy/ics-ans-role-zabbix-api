# ics-ans-role-zabbix-api

Ansible role to install zabbix-api.

## Role Variables

```yaml
zabbix_api_server: http://zabbix-frontend # OR https://zabbix-frontend
zabbix_api_user: Admin
zabbix_api_pass: zabbix
zabbix_api_descriptin: "zabbix proxy"
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-zabbix-api
```

## License

BSD 2-clause
